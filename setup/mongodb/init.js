conn = new Mongo();
db = conn.getDB("via");

db.carousel_pics.createIndex({ image_url: 1 }, { unique: true });

var carouselData = [
  {
    file_name: "h1.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/h1.jpg",
    latitude: "38.8893715",
    longitude: "-77.0352269",
  },
  {
    file_name: "h2.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/h2.jpg",
    latitude: 41.8825932,
    longitude: -87.623303,
  },
  {
    file_name: "h3.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/h3.jpg",
    latitude: 42.3587051,
    longitude: -71.0573013,
  },
  {
    file_name: "h4.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/h4.jpg",
    latitude: 38.9075885,
    longitude: -77.0634538,
  },
  {
    file_name: "m1.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/m1.jpg",
    latitude: 18.528580755276284,
    longitude: 120.71830331648961,
  },
  {
    file_name: "m2.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/m2.jpg",
    latitude: 14.793530444525654,
    longitude: 120.95324972623848,
  },
  {
    file_name: "m3.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/m3.jpg",
    latitude: 15.304788505736914,
    longitude: 120.7402833692842,
  },
  {
    file_name: "m4.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/m4.jpg",
    latitude: 1.282302,
    longitude: 103.858528,
  },
  {
    file_name: "r1.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/r1.jpg",
    latitude: 39.6655,
    longitude: -105.2052,
  },
  {
    file_name: "r2.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/r2.jpg",
    latitude: 32.7707,
    longitude: -117.2514,
  },
  {
    file_name: "r3.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/r3.jpg",
    latitude: 40.7128,
    longitude: -74.006,
  },
  {
    file_name: "r4.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/r4.jpg",
    latitude: 33.985,
    longitude: -118.4695,
  },
  {
    file_name: "t1.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/t1.jpg",
    latitude: 35.032412,
    longitude: -111.02202,
  },
  {
    file_name: "t2.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/t2.jpg",
    latitude: 36.512098,
    longitude: -75.859911,
  },
  {
    file_name: "t3.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/t3.jpg",
    latitude: 36.903631,
    longitude: -111.414083,
  },
  {
    file_name: "t4.jpg",
    image_url: "https://viaphotos.s3.us-east-2.amazonaws.com/t4.jpg",
    latitude: 36.99898,
    longitude: -109.045175,
  },
];
db.carousel_pics.insertMany(carouselData);

print("Initialization completed successfully.");
