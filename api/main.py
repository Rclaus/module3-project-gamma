from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import MyAuthenticator
from routers import accounts, photos, profile_pics, carousel


app = FastAPI()

authenticator_instance = MyAuthenticator(os.getenv("SIGNING_KEY"))

app.include_router(authenticator_instance.router)
app.include_router(accounts.router)
app.include_router(photos.router)
app.include_router(profile_pics.router)
app.include_router(carousel.router)

origins = [
    "http://localhost:3000",
    "https://destination-dev.gitlab.io/module3-project-gamma",
    "https://destination-dev.gitlab.io",
    "https://destination-dev.gitlab.io/module3-project-gamma/",
    os.environ.get("CORS_HOST"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "Welcome to Via!"}
