from main import app
from queries.profile_pics import ProfilePicQueries
from fastapi.testclient import TestClient

client = TestClient(app)


class CreateProfilePicDoc:
    def get_by_username(self, username):
        return None

    def create(self, profile_pic):
        result = {
            "file_name": "ab@com",
            "username": "ab@com",
            "image_url": "whatever.jpg",
        }
        return result


def test_create_profile_pic_doc():
    app.dependency_overrides[ProfilePicQueries] = CreateProfilePicDoc
    request_data = {
        "file_name": "ab@com",
        "username": "ab@com",
        "image_url": "whatever.jpg",
    }

    response = client.post("/api/profile-pics", json=request_data)

    app.dependency_overrides = {}

    assert response.status_code == 201
    assert response.json() == request_data
