from fastapi.testclient import TestClient
from main import app
from queries.carousel import CarouselQueries

client = TestClient(app)


class TestCarouselQueries:
    def get_all_carousel(self):
        return []


def test_get_all_carousel():
    app.dependency_overrides[CarouselQueries] = TestCarouselQueries
    response = client.get("/api/carousels/all")
    assert response.status_code == 200
    assert response.json() == {"carousels": []}
