from fastapi.testclient import TestClient
from main import app
from queries.photos import PhotoQueries

client = TestClient(app)


class EmptyPhotosQueries:
    def get_all_photos(self):
        return []


def test_get_all_photos():
    app.dependency_overrides[PhotoQueries] = EmptyPhotosQueries

    response = client.get("/api/photos/all")

    assert response.status_code == 200
    assert response.json() == {"photos": []}
