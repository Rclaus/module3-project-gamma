from fastapi.testclient import TestClient
from main import app
from queries.photos import PhotoQueries

client = TestClient(app)


class MockPhotoQuery:
    def get_by_file_name(self, file_name):
        return None

    def create(self, photo):
        result = {
            "file_name": "test@gmail.com_test.jpg",
            "username": "test@gmail.com",
            "image_url": "test.jpg",
            "description": None,
            "latitude": None,
            "location": None,
            "longitude": None,
            "timestamp": None,
        }
        return result


def test_create_photo():
    # Arrange
    app.dependency_overrides[PhotoQueries] = MockPhotoQuery
    request_data = {
        "file_name": "test@gmail.com_test.jpg",
        "username": "test@gmail.com",
        "image_url": "test.jpg",
        "description": None,
        "latitude": None,
        "location": None,
        "longitude": None,
        "timestamp": None,
    }

    # Act
    response = client.post("/api/photos", json=request_data)

    # Clean up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 201
    assert response.json() == request_data
    print("GREAT SUCCESS!")
