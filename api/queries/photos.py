from queries.client import Queries
from models.photos import PhotoCreate, PhotoUpdate, PhotoOut
from typing import List


class PhotoQueries(Queries):
    DB_NAME = "via"
    COLLECTION = "photos"

    def is_duplicate_photo(self, photo: PhotoCreate):
        existing_photo = self.collection.find_one(
            {"file_name": photo.file_name}
        )
        return existing_photo is not None

    def create(self, photo: PhotoCreate) -> PhotoOut:
        if self.is_duplicate_photo(photo):
            return None
        props = photo.dict(exclude_unset=True)
        result = self.collection.insert_one(props)
        if result.acknowledged:
            return PhotoOut(**props)
        return None

    def update(self, file_name: str, photo: PhotoUpdate) -> PhotoOut:
        props = photo.dict(exclude_unset=True)
        result = self.collection.update_one(
            {"file_name": file_name}, {"$set": props}
        )
        if result.modified_count:
            return self.get_by_file_name(file_name)
        return None

    def get_by_file_name(self, file_name: str) -> PhotoOut:
        props = self.collection.find_one({"file_name": file_name}, {"_id": 0})
        if props:
            return PhotoOut(**props)
        return None

    def get_all_by_username(self, username: str) -> List[PhotoOut]:
        props = self.collection.find({"username": username}, {"_id": 0})
        return [PhotoOut(**p) for p in props]

    def delete(self, file_name: str) -> bool:
        result = self.collection.delete_one({"file_name": file_name})
        return result.deleted_count > 0

    def get_all_photos(self) -> List[PhotoOut]:
        props = self.collection.find({}, {"_id": 0})
        photos = [PhotoOut(**p) for p in props]
        return photos
