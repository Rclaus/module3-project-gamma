from queries.client import Queries
from models.accounts import (
    Account,
    AccountIn,
    AccountOutWithPassword,
)
from pymongo.errors import DuplicateKeyError


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "via"
    COLLECTION = "accounts"

    def is_duplicate_username(self, account: AccountIn):
        existing_account = self.collection.find_one(
            {"username": account.username}
        )
        return existing_account is not None

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        if self.is_duplicate_username(info):
            raise DuplicateAccountError()
        props = info.dict()
        props["password"] = hashed_password
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicateAccountError()
        props["id"] = str(props["_id"])
        return Account(**props)

    def get(self, username: str) -> AccountOutWithPassword:
        props = self.collection.find_one({"username": username})
        if not props:
            return None
        props["id"] = str(props["_id"])
        props["hashed_password"] = props.pop("password")
        return AccountOutWithPassword(**props)

    def update_password(
        self,
        username: str,
        hashed_password: str,
    ) -> AccountOutWithPassword:
        result = self.collection.update_one(
            {"username": username},
            {"$set": {"password": hashed_password}},
        )
        if result.modified_count == 0:
            return None
        return self.get(username)
