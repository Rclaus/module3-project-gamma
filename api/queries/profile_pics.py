from queries.client import Queries
from models.profile_pics import (
    ProfilePicCreate,
    ProfilePicUpdate,
    ProfilePicOut,
)


class ProfilePicQueries(Queries):
    DB_NAME = "via"
    COLLECTION = "profile_pics"

    def is_duplicate_profile_pic(self, profile_pic: ProfilePicCreate):
        existing_profile_pic = self.collection.find_one(
            {"username": profile_pic.username}
        )
        return existing_profile_pic is not None

    def create(self, profile_pic: ProfilePicCreate) -> ProfilePicOut:
        if self.is_duplicate_profile_pic(profile_pic):
            return None
        props = profile_pic.dict(exclude_unset=True)
        result = self.collection.insert_one(props)
        if result.acknowledged:
            return ProfilePicOut(**props)
        return None

    def update(
        self, username: str, profile_pic: ProfilePicUpdate
    ) -> ProfilePicOut:
        props = profile_pic.dict(exclude_unset=True)
        result = self.collection.update_one(
            {"username": username}, {"$set": props}
        )
        if result.modified_count:
            return self.get_by_username(username)
        return None

    def get_by_username(self, username: str) -> ProfilePicOut:
        props = self.collection.find_one({"username": username}, {"_id": 0})
        if props:
            return ProfilePicOut(**props)
        return None

    def delete(self, username: str) -> bool:
        result = self.collection.delete_one({"username": username})
        return result.deleted_count > 0
