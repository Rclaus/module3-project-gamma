from queries.client import Queries
from models.carousel import CarouselOut, CarouselCreate
from typing import List


class CarouselQueries(Queries):
    DB_NAME = "via"
    COLLECTION = "carousel_pics"

    def create(self, carousel: CarouselCreate) -> CarouselOut:
        props = carousel.dict(exclude_unset=True)
        result = self.collection.insert_one(props)
        if result.acknowledged:
            return CarouselOut(**props)
        return None

    def get_all_carousel(self) -> List[CarouselOut]:
        props = self.collection.find({}, {"_id": 0})
        carousels = [CarouselOut(**p) for p in props]
        return carousels
