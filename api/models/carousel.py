from pydantic import BaseModel
from typing import Optional, List


class CarouselBase(BaseModel):
    file_name: str
    image_url: str
    latitude: str
    longitude: str


class CarouselCreate(CarouselBase):
    file_name: Optional[str] = None
    image_url: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None


class CarouselOut(CarouselBase):
    file_name: Optional[str] = None
    image_url: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None


class CarouselInDB(CarouselOut):
    pass


class CarouselInResponse(BaseModel):
    carousels: List[CarouselOut]
