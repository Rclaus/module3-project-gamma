from pydantic import BaseModel
from typing import Optional, List
from datetime import datetime


class PhotoBase(BaseModel):
    file_name: str
    username: str
    image_url: str


class PhotoCreate(PhotoBase):
    description: Optional[str] = None
    location: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None
    timestamp: Optional[datetime] = None


class PhotoUpdate(BaseModel):
    description: Optional[str] = None
    location: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None
    timestamp: Optional[datetime] = None


class PhotoOut(PhotoBase):
    description: Optional[str] = None
    location: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None
    timestamp: Optional[datetime] = None


class PhotoInDB(PhotoOut):
    pass


class PhotoInResponse(BaseModel):
    photos: List[PhotoOut]
