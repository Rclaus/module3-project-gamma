from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import Optional


class AccountIn(BaseModel):
    username: str
    password: str
    full_name: str


class AccountOut(BaseModel):
    id: str
    username: str
    full_name: str


class AccountOutWithPassword(AccountOut):
    hashed_password: Optional[str]


class AccountUpdate(BaseModel):
    password: str


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError as e:
                raise e(f"Not a valid object id: {value}")
            except Exception as e:
                raise e(str(e))
        return value


class Account(AccountIn):
    id: PydanticObjectId
    hashed_password: Optional[str]
