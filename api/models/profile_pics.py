from pydantic import BaseModel


class ProfilePicBase(BaseModel):
    file_name: str
    username: str
    image_url: str


class ProfilePicCreate(ProfilePicBase):
    pass


class ProfilePicUpdate(BaseModel):
    image_url: str


class ProfilePicOut(ProfilePicBase):
    pass


class ProfilePicInResponse(BaseModel):
    photo: ProfilePicOut
