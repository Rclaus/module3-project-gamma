from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from typing import Optional
from queries.accounts import AccountQueries, DuplicateAccountError
from models.accounts import AccountIn, AccountUpdate, AccountOutWithPassword


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOutWithPassword
    hashed_password: Optional[str]


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOutWithPassword = Depends(
        authenticator.try_get_current_account_data
    ),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return AccountToken(
            account=AccountOutWithPassword(**account),
            hashed_password=account.get("hashed_password"),
            access_token=request.cookies[authenticator.cookie_name],
            type="Bearer",
        )


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return (
        AccountToken(
            account=account, hashed_password=hashed_password, **token.dict()
        )
        if token
        else HttpError(detail="Failed to retrieve token")
    )


@router.get("/api/accounts/{username}", response_model=AccountOutWithPassword)
def get_by_username(username: str, queries: AccountQueries = Depends()):
    return queries.get(username)


@router.put("/api/accounts/{username}", response_model=AccountOutWithPassword)
async def update_password(
    username: str,
    password_update: AccountUpdate,
    current_account_data: dict = Depends(
        authenticator.get_current_account_data
    ),
    accounts: AccountQueries = Depends(),
):
    current_username = current_account_data.get("username")

    if current_username != username:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Access denied",
        )

    account = await accounts.get(current_username)

    if not account:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Account not found",
        )

    hashed_password = authenticator.hash_password(password_update.password)
    account = await accounts.update_password(current_username, hashed_password)

    if not account:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Update password failed",
        )

    return account
