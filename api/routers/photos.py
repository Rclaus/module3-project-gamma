from fastapi import APIRouter, Depends, status, HTTPException
from queries.photos import PhotoQueries
from models.photos import (
    PhotoCreate,
    PhotoUpdate,
    PhotoOut,
    PhotoInResponse,
)

router = APIRouter()


@router.post(
    "/api/photos",
    response_model=PhotoOut,
    status_code=status.HTTP_201_CREATED,
)
def create_photo(
    photo: PhotoCreate,
    queries: PhotoQueries = Depends(),
):
    existing_photo = queries.get_by_file_name(photo.file_name)
    if existing_photo is not None:
        return existing_photo

    created_photo = queries.create(photo)
    if created_photo is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Failed to create photo",
        )
    return created_photo


@router.get(
    "/api/photos/all",
    response_model=PhotoInResponse,
)
def get_all_photos(queries: PhotoQueries = Depends()):
    photos = queries.get_all_photos()
    return {"photos": photos}


@router.get(
    "/api/photos/all/username/{username}",
    response_model=PhotoInResponse,
)
def get_all_photos_by_user(username: str, queries: PhotoQueries = Depends()):
    photos = queries.get_all_by_username(username)
    return {"photos": photos}


@router.get(
    "/api/photos/{file_name}",
    response_model=PhotoOut,
)
def get_photo(
    file_name: str,
    queries: PhotoQueries = Depends(),
):
    photo = queries.get_by_file_name(file_name)
    if not photo:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Photo not found",
        )
    return photo


@router.put(
    "/api/photos/{file_name}",
    response_model=PhotoOut,
)
def update_photo(
    file_name: str,
    photo: PhotoUpdate,
    queries: PhotoQueries = Depends(),
):
    existing_photo = queries.get_by_file_name(file_name)
    if not existing_photo:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Photo not found",
        )
    return queries.update(file_name, photo)


@router.delete(
    "/api/photos/{file_name}",
)
def delete_photo(
    file_name: str,
    queries: PhotoQueries = Depends(),
):
    existing_photo = queries.get_by_file_name(file_name)
    if not existing_photo:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Photo not found",
        )
    queries.delete(file_name)
    return {"message": "Photo deleted successfully"}
