from fastapi import APIRouter, Depends, status, HTTPException
from queries.profile_pics import ProfilePicQueries
from models.profile_pics import (
    ProfilePicCreate,
    ProfilePicOut,
    ProfilePicUpdate,
)


router = APIRouter()


@router.post(
    "/api/profile-pics",
    response_model=ProfilePicOut,
    status_code=status.HTTP_201_CREATED,
)
def create_profile_pic(
    profile_pic: ProfilePicCreate,
    queries: ProfilePicQueries = Depends(),
):
    existing_profile_pic = queries.get_by_username(profile_pic.username)
    if existing_profile_pic is not None:
        return existing_profile_pic

    created_profile_pic = queries.create(profile_pic)
    if created_profile_pic is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Failed to create profile picture",
        )
    return created_profile_pic


@router.get(
    "/api/profile-pics/{username}",
    response_model=ProfilePicOut,
)
def get_profile_pic(
    username: str,
    queries: ProfilePicQueries = Depends(),
):
    profile_pic = queries.get_by_username(username)
    if not profile_pic:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Profile picture not found",
        )
    return profile_pic


@router.put(
    "/api/profile-pics/{username}",
    response_model=ProfilePicOut,
)
def update_profile_pic(
    username: str,
    profile_pic: ProfilePicUpdate,
    queries: ProfilePicQueries = Depends(),
):
    updated_profile_pic = queries.update(username, profile_pic)
    if not updated_profile_pic:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Profile picture not found",
        )
    return updated_profile_pic


@router.delete(
    "/api/profile-pics/{username}",
)
def delete_profile_pic(
    username: str,
    queries: ProfilePicQueries = Depends(),
):
    existing_profile_pic = queries.get_by_username(username)
    if not existing_profile_pic:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Profile picture not found",
        )
    queries.delete(username)
    return {"message": "Profile picture deleted successfully"}
