from fastapi import APIRouter, Depends, status, HTTPException
from queries.carousel import CarouselQueries
from models.carousel import CarouselCreate, CarouselOut, CarouselInResponse

router = APIRouter()


@router.get(
    "/api/carousels/all",
    response_model=CarouselInResponse,
)
def get_all_carousel(queries: CarouselQueries = Depends()):
    carousels = queries.get_all_carousel()
    return {"carousels": carousels}


@router.post(
    "/api/carousels",
    response_model=CarouselOut,
    status_code=status.HTTP_201_CREATED,
)
def create_carousel(
    photo: CarouselCreate,
    queries: CarouselQueries = Depends(),
):
    created_photo = queries.create(photo)
    if created_photo is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Failed to create photo",
        )
    return created_photo
