import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountQueries
from models.accounts import AccountOut, AccountOutWithPassword


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: AccountQueries,
    ):
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: AccountQueries = Depends(use_cache=False),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithPassword):
        return account.hashed_password

    def get_account_data_for_cookie(self, account: AccountOut):
        return account.username, AccountOut(**account.dict())

    async def login(self, response, request, form, accounts):
        hashed_password = self.hash_password(form.password)
        accounts.create(form, hashed_password)

        token = self.create_token(
            subject=form.username,
            audience=request.client.host,
            username=form.username,
            hashed_password=hashed_password,
        )

        response.set_cookie(
            name=self.cookie_name,
            value=token,
            max_age=self.cookie_max_age,
            path=self.cookie_path,
            domain=self.cookie_domain,
            secure=self.cookie_secure,
            httponly=self.cookie_httponly,
            samesite=self.cookie_samesite,
        )

        return {"access_token": token, "type": "Bearer"}


authenticator = MyAuthenticator(os.environ.get("SIGNING_KEY"))
