# Integrations

The VIA application needs to get data from the following third-party APIs:

- Map data utilizing the Google Maps API

- Cloud file storage utilizing the Amazon S3 API
