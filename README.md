# VIA

- Huilin Li
- Matt Hoadley
- Rob Claus
- Thomas Laurick

VIA is an innovative app designed to revolutionize the way users interact with photos and location data. Our platform provides a seamless and intuitive experience, allowing individuals to effortlessly upload their photos, share them with others, and access valuable information associated with each image's location. With VIA, users can capture and store their favorite moments, whether it's an awe-inspiring landscape, a delicious meal, or a cherished memory. Our app combines the power of geolocation technology with a vibrant community, enabling users to not only share their experiences but also discover hidden gems, popular landmarks, and local insights.

## Design

- [Wireframes](docs/wireframes)
- [Integrations](docs/integrations.md)

## Deployed URLs

- [Frontend](https://destination-dev.gitlab.io/module3-project-gamma/)
- [Backend](https://mar-9-et-marptapi.mod3projects.com/)

## Intended Market

Our intended market is adventurous individuals who love snapping photos while they explore, whether in the community or afar, as well as those who just enjoy using photography to capture memories and moments on the go.

## Functionality

All visitors to our site will be greeted by our landing page displaying a snapshot of the functionality that our application offers, to include a brief description of our service as well as a interactive map displaying pins and a corresponding photo carousel.

- Visitors will be able to register to create an account
- After registration, they will be able to perform the following actions:

1. Log in to their personnel profile and photo collection
2. Upload photos to the service to be stored in the cloud
3. View collection of photos uploaded by the user with a corresponding map display showing the location where the photos were taken
4. When user selects a photo pin on the map, the scrolling image field and carousel will snap to the photo pin selected and vice versa.
5. Navigate through images uploaded by the user via a carousel

## Project Initialization

In order to experience the full functionality of the VIA application for yourself, follow these steps:

1. Clone the repository onto your local machine
2. CD into the new project directory
3. Run `docker volume create mongo-data`
4. Run `docker compose build`
5. Run `docker compose up`
6. Run `docker exec -it module3-project-gamma-api-1 bash`
7. Run `python main.py loaddata carousel.json` to load homepage, map pins, and carousel photos.
8. Exit the container's CLI and begin making memories with VIA!
