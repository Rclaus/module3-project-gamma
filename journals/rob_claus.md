July 24, 2023
Got the carousel and the list of photos all to work with onClick methods and snaps the pins and the photos to the image and everything is working.

July 20, 2023
Got the maps up and running, the pins are linked to the photos and pulling that latitude longitude.

July 19, 2023
Started to get maps up and running, would not display pins and giving a hard time routing.

July 18, 2023
Went and got rid of leaflet and decided to use google maps since it has great documentation. Attempted to implement.

July 17, 2023
deploy leaflet, got the map up and running. Figured out it does not have good versatility
fix carousel size

July 14, 2023
refactoring everyone's code

July 13, 2023
research s3 bucket
organized code from all teammates

July 12, 2023
Worked on frontend authenticator , token for the front end user photo view page.

July 11, 2023
figured the best method to store our credentials, we put them in a config.json and added it to the git ignore.

July 10, 2023
Signed up for Amazon S3, created IAM admin account so we dont use my root account. Created amazon S3 bucket and secret key
deployed amazon S3 to react frontend so that user can upload photo

June 30, 2023
Determined what storage we are going to use, determing S3 would be the best route. Determining how we are going to pull lat/lon and have the smoothest method.

June 29, 2023
Finished creating the database fully implemented JWT for authentication for end points and users. Signed in successfully.

June 28, 2023
Got JWTdown completed in the morning time WOOHOO, We started implementing MongoDB in the backend and attempted to setup MongoCompass.

June 27, 2023
setup docker-compose.yaml and requirement.txt, Attempted to do the the JWTdown authentication, it was being a little bit of a pain.

June 26, 2023
Created multiple issues in Git to align with our features.

June 23, 2023
We decided we are going to use Amazon S3 for storing images as it is the best method with a lot of documentation

June 22, 2023
Discussed more details of what backend we want to use for a database between sql and Mongo.

June 21, 2023
Discussed more things we wanted to implement and the design of the pages, identified MVP and stretch goals. We will alter the MVP as we go along.

June 20, 2023
