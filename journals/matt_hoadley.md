28 July 2023- Combined branch files including journal and README with carousel feature branch to be merged with main. Implement updated CSS styling. Continued work on CI/CD in order deploy site.

27 July 2023- Began working on Readme.md, created integrations.md. Continued working on front end site visuals.

26 July 2023- Worked on unit tests, made improvements to site visuals.

25 July 2023- Created withAuthProtection.js and modifies App.js to created authenticated routes for user gallery and upload photo form. Created backend tests test_upload_photo, test_photos, and test_createprofilepicedoc.

24 July 2023- Created JWTDManager.js to protect URL routes and changed imports and exports for affected components.

23 July 2023- Merged to incorporate functioning protected routes feature.

20 July 2023- Consolidated code, updated routers and queries, refactored code for AWS, modified UserGallery to organize photos next to Google Maps.

19 July 2023- Managed to render Google Maps API with hard coded coordinates. Consolidated code from features branches and merged feature branches with main.

18 July 2023- Completes merge request and closes issues 1 and 2.

17 July 2023- Integrates my codebase with updates from team mates. Updates config to successfully upload photos. Began writing merge request to close issues 1 and 2.

14 July 2023- Updated styling for Nav.js and Main.js with carousel and updated layout.

13 July 2023- Group updated styling for frontend and implemented modals for login and register forms.

12 July 2023- Group continued working on frontend react components and began styling on components.

11 July 2023- Group continued working on frontend react components and created Login.js, Register.js, and userPhotosView.js.

10 July 2023- Began creating front-end for application. We were also able to successfully set up the Amazon S3 3rd party API endpoint and submit images to it from our web application front end.

30 June 2023- Group coded to continue creating routes and queries.

29 June 2023- Group coded and created and populated models, queries, and routers directories. Created authenticator.py. jwtdown working properly on the back end with log in, log out, create user, and issue token. Docker-compose.yaml edited with Mongo port assignment and MongoDB Compass now running.

28 June 2023- Brainstormed as a group for how to implement MongoDB and authentication. Broke out individually to explore and implement features. Created accounts, queries, routers directories and created **init**.py for queries and routers but got stuck trying to separate mongoDB config from authentication.

27 June 2023- Continued working with group to create user issues in Git; began building docker-compose.yaml, requirements.txt, dockerfile, and dockerfile.dev. Also, we managed to get React and FastAPI servers up and running with deadline showing on the React homepage.

26 June 2023- Continued working with group to create user issues in Git.

23 June 2023- Collaborated with group to create API query structure.

22 June 2023- Collaborated with group to begin creating potential user issues to be worked out.

21 June 2023- Collaborated with group to refine wireframe and proposed features.

20 June 2023- Collaborate with group to generate idea, features, and create wireframe for project
