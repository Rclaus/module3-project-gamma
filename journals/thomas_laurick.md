27 July 2023: Today we finished the main page with all of the features working. It was a real team effort and it was a lot of fun getting through the blockers. Rob led the CI and walked through it with all of us so we knew what was going on. Afterwards I added code to the main page and user gallery page so that the bounds of the rendered map would include all of the pins for the images returned by the get request. Other than that, we are wrapping things up with the readme and making corrections/refactoring as needed.

26 July 2023: Today I created my test. I let the team choose theirs first as I felt I would be fine creating whatever was left, but still found it challenging. I definitely need practice writing tests. We decided to use custom pins for the maps and so we created some with varied colors that we would like to make available for selection with a stretch goal. We also all selected some photos to use for display on the main page of our app. Rather than hosting the images and making api calls to load the main page we have the compressed and resized images in our public folder and I created a json document that we are using to load the carousel_pics documents into the via database.

25 July 2023: Today I modified the ExifExtractor.js to assign a default lat/lng for images that don't contain exif data. This will allow a user's uploaded photos to still appear in the thumbnails, carousel, and on the map. The user will be able to update the lat/lng to get it to render in a custom location. Matt and I worked on url protection. It works well, but is also preventing refreshing.

24 July 2023: Today was mainly moving large and/or frequently code blocks into their own files to both compartmentalize the code and reduce the file size of our elements/components. Only other significant change was to create a new collection for the profile pics which of course also required new models, routers, and queries. This allows us to use a get all by username router and query without needing to filter out the profile picture from the images to be displayed in thumbnails/carousel or pins to be rendered on the map.

20 July 2023: Today we mainly consolidated code, made some updates to our routers and queries, refactored our code to store the AWS credentials into the .env, and modify our UserGallery to get the coordinates container moved to the left of the rendered Google Maps API map.

19 July 2023: We now have the map from Google Maps API rendered and plotting hard coded coordinates. Moved the coordinates on the UI to the left side of the map. Next step is to replace the coordinates on the UI with real coordinates from the database using an API call with the end-goal of displaying thumbnails that retain the interactive property with the map pins. A lot of time today has also been spent consolidating code and a merge request completed adding the exif_extractor branch to the main branch.

18July 2023: Today we were able to get the API for creating a photo document using Register.js to work. It required refactoring the photos models and the query and router for the api/photo path. We did not update the rest of the routers or queries for now. We were also able to get the ImageUploadForm to successfully pull the username by using the JWT Token, rename the image selected for upload by the user to "username/filename.jpg", extract the exif data and pull the latitude, longitude, and datetimeorigin, upload the renamed photo to a bucket in the user's "folder", receive a response with the URL for the photo, and then create a photos document with all of the above information. For photos that are uploaded without exif data, the user is prompted to update any info related to location as desired from within their gallery. All other processing still occurs. It was quite a productive day!

17 July 2023: Today we split into pairs to work on two major features. Matt and I are working on the EXIF extractor while Rob and Huilin are working on getting the Map to display on the UI. Matt is also working on a merge request. We decided to require users to signup with their email as their username. This will provide a unique id with which we should be able to name, filter, and search/get from our photos collection. Right now the challenge is modifying the Register.js so that when a user creates an account we make an initial api call to the database to not only create their account, but to create a document in the photos collection consisting of a default profile picture, their username, and the image url. Once we have the API endpoint working we will update the ImageUploadform.

15 July 2023: Today I was able to troubleshoot our 401 POST error to the /Token endpoint. Our team agreed yesterday that it was fine for me to do so. I pushed the updated code to GitLab and let everyone know the status in our Slack Team DM. The next step is to turn the ImageUploader.js into a modal on the UserPhoto.js element (may be renamed soon).

14 July 2023: Today we mainly worked on consolidating code from each person into a single product. Goals for this weekend are to resolve the non-critical 401 POST error to the /Token endpoint, work on the modal components within the Nav.js, and review docs for python pillow.

13 July 2023: Wow, a lot accomplished. Still troubleshooting the 401 response from the POST request to the /token endpoint. Still no noticeable deprication from the issue. Main.js has been refactored with a drop down form to register or login. Bootstrap grids were added to it as well as a functional carousel at the bottom. Our Login.js and Register.js forms were fabulously transformed into modals that helped bring the form to life.

12 July 2023: Successfully implemented JWTdown Authentication for React. Able to register, log in, and log out. More front-end code continuously being written. Troubleshooting a 401 response from a POST request to the /token endpoint. It is a duplicate POST request and doesn't impact functionality. Oddly, we did have to add "npm audit fix" to our package.json as a script "postinstall" in order to compile successfully and get the JWTdown Authentication for React to work.

11 July 2023: Last night was able to find a solution for the S3 credentials. Put them in a config.json file and added the path to our environment while also adding it to .gitignore. Started working on the front end elements and components. Started working on front-end JWTdown Authentication for React.

10 July 2023: Today we signed up for AWS S3 service, started working on our front-end code, and successfully uploaded images to our S3 bucket. Unfortunately we are having a challenge with the credentials not working when putting them into the .env and referencing them from the docker-compose.yaml. We also tried using the S3 documentation and putting them in a .aws/config file with no joy.

30 June 2023: Refactored code by combining all of the Account models into models/accounts.py Verified endpoints and JWTdown Auth still worked after refactoring. Learned that there is a boto3 python library for AWS that will simplify our code. Decided to use a free geocoding site such as Nominatim to populate a city, state or poi in the location portion of the UI instead of using up an additional api request with Google Maps API.

29 June 2023: Finished creating the "via" database with the "Accounts" collection. Fully implemented JWTdown Authentication for the Accounts endpoints. Successfully created a user, signed in, and signed out with token validation. I created my first merge request and learned how to link issues to the description within the GitLab merge request form.

28 June 2023: Modified project to use a single back-end API. Also implemented changes to incorporate JWTdown. This required building some database structure. The group did a code-along and I took some time to troubleshoot the errors that prevented our project from compiling without errors.

27 June 2023: Created containers for project including a new container for mongodb. Still working on JWTdown authentication feature.

26 June 2023: Created multiple issues within GIT for the project.

23 June 2023: Determined that we will use Amazon AWS which includes S3 for hosting our project. Covered pros and cons for using PostgreSQL and MongoDB. Will make our decision hopefully by the 27th of June.

22 June 2023: Discussed some of the details related to whether we use PostgreSQL or Mongo and Pymongo for the backend.

21 June 2023: Discussed more items in our wireframe. We also have started looking further into how we will accomplish some of the features as well as identifying stretch goals.

20 June 2023: Met with my awesome teammates. We were able to quickly agree on a project and started wireframing our user interface. We've already identified features and received a green light from Delonte.
