import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import withAuthProtection from "./withAuthProtection";
import Nav from "./Nav";
import UserGallery from "./UserGallery";
import ImageUploadForm from "./ImageUploadForm";
import Main from "./Main";
import Footer from "./Footer";
import "./css/App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

const ProtectedUserGallery = withAuthProtection(UserGallery);
const ProtectedImageUploadForm = withAuthProtection(ImageUploadForm);
const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");

function App() {
  return (
    <div>
      <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
        <BrowserRouter basename={basename}>
          <Nav />
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/gallery" element={<ProtectedUserGallery />} />
            <Route
              path="/upload/photo"
              element={<ProtectedImageUploadForm />}
            />
          </Routes>
          <Footer />
        </BrowserRouter>
      </AuthProvider>
    </div>
  );
}

export default App;
