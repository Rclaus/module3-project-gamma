import "./css/Card.css";
import React from "react";

function Card() {
  return (
    <div className="ag-offer-block">
      <div className="team">EXPLORE WHAT WE CAN DO</div>
      <div className="ag-format-container">
        <ul className="ag-offer_list">
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-1.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">Look for photos on map</div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                Explore captivating photos on an interactive map. Search and
                discover images taken in specific locations for a visually
                engaging experience.
              </p>
            </div>
          </li>
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-2.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">Search places</div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                Search and explore places of interest, finding essential details
                like addresses, reviews, and ratings for a seamless discovery
                experience.
              </p>
            </div>
          </li>
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-3.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">
                Upload your favorite event photo
              </div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                Upload your favorite event photos and cherish special moments
                with ease.
              </p>
            </div>
          </li>
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-4.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">Connect with your friends</div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                The website allows you to connect with your friends, fostering
                meaningful relationships and facilitating seamless communication
                and interaction.
              </p>
            </div>
          </li>
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-5.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">Mark down memory</div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                Marking down memories, preserving special moments and creating
                lasting impressions.
              </p>
            </div>
          </li>
          <li className="ag-offer_item">
            <div className="ag-offer_visible-item">
              <div className="ag-offer_img-box">
                <img
                  src="https://raw.githack.com/SochavaAG/example-mycode/master/pens/1_images/icon-64-6.svg"
                  className="ag-offer_img"
                  alt=""
                />
              </div>
              <div className="ag-offer_title">Check photo's street view</div>
            </div>
            <div className="ag-offer_hidden-item">
              <p className="ag-offer_text">
                The website lets you check photos with a street view, offering a
                new perspective and immersive experience of captured moments.
              </p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Card;
