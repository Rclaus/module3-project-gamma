import { useState } from "react";
import { Modal, Button, Form } from "react-bootstrap";

const UpdatePhoto = ({ markers }) => {
  const [show, setShow] = useState(false);
  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState("");
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");

  const handleClose = () => {
    setShow(false);
    setSelectedPhoto(null);
    setDescription("");
    setLocation("");
    setLatitude("");
    setLongitude("");
  };

  const handleShow = () => setShow(true);

  const handleSelectPhoto = (photo) => {
    setSelectedPhoto(photo);
    setDescription(photo.description || "");
    setLocation(photo.location || "");
    setLatitude(photo.latitude);
    setLongitude(photo.longitude);
  };

  const handleDeletePhoto = () => {
    const requestOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `${process.env.REACT_APP_API_HOST}/api/photos/${encodeURIComponent(
        selectedPhoto.file_name
      )}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      })
      .finally(() => {
        handleClose();
      });
  };

  const handleUpdatePhoto = () => {
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        description,
        location,
        latitude,
        longitude,
      }),
    };

    fetch(
      `${process.env.REACT_APP_API_HOST}/api/photos/${encodeURIComponent(
        selectedPhoto.file_name
      )}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      })
      .finally(() => {
        handleClose();
      });
  };

  // The button will be disabled if none of the fields have been filled
  const isButtonDisabled = !(description || location || latitude || longitude);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Update Photo
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Photo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {markers.map((marker, index) => (
            <div key={index} onClick={() => handleSelectPhoto(marker)}>
              <img
                src={marker.img}
                alt={marker.file_name}
                style={{
                  maxWidth: "100px",
                  maxHeight: "100px",
                  padding: "5px",
                }}
              />
            </div>
          ))}
          {selectedPhoto && (
            <Form>
              <Form.Group controlId="formUpdatePhotoDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formUpdatePhotoLocation">
                <Form.Label>Location</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter location"
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formUpdatePhotoLatitude">
                <Form.Label>Latitude</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Edit latitude"
                  value={latitude}
                  onChange={(e) => setLatitude(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formUpdatePhotoLongitude">
                <Form.Label>Longitude</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Edit longitude"
                  value={longitude}
                  onChange={(e) => setLongitude(e.target.value)}
                />
              </Form.Group>
            </Form>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={handleDeletePhoto}>
            Delete
          </Button>
          <Button
            variant="primary"
            onClick={handleUpdatePhoto}
            disabled={isButtonDisabled}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default UpdatePhoto;
