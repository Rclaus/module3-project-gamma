import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { uploadToS3 } from "./photoProcessing/UploadToS3";

function UserUpdateForm() {
  const { token } = useToken();
  const [accountData, setAccountData] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [file, setFile] = useState(null);
  const [isProcessing, setIsProcessing] = useState(false);
  const [password, setPassword] = useState("");

  useEffect(() => {
    if (token) {
      const fetchAccountData = async () => {
        const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
        const response = await fetch(accountUrl, {
          credentials: "include",
        }).then((response) => response.json());
        setAccountData(response.account);
      };
      fetchAccountData();
    }
  }, [token]);

  const handlePasswordUpdate = async () => {
    if (accountData) {
      const updateUrl = `${process.env.REACT_APP_API_HOST}/api/accounts/${accountData.username}`;
      await fetch(updateUrl, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ password }),
      });
      setPassword("");
    } else {
      console.error("Account data not available for password update.");
    }
  };

  const handleFileSelect = (event) => {
    setFile(event.target.files[0]);
    setIsProcessing(true);
  };

  const uploadFile = async () => {
    const imageUrl = await uploadToS3(file);
    setImageUrl(imageUrl);
    setIsProcessing(false);
  };

  return (
    <>
      <div>
        <h1>Test Image Upload</h1>
        <input type="file" onChange={handleFileSelect} />
        {file && (
          <div style={{ marginTop: "10px" }}>
            <button onClick={uploadFile} disabled={isProcessing}>
              {isProcessing ? "Processing..." : "Process and Upload"}
            </button>
          </div>
        )}
        {imageUrl && (
          <div style={{ marginTop: "10px" }}>
            <img
              src={imageUrl}
              alt="uploaded"
              style={{
                maxWidth: "300px",
                maxHeight: "300px",
                width: "auto",
                height: "auto",
              }}
            />
          </div>
        )}
      </div>

      <div>
        <label htmlFor="password">New Password:</label>
        <input
          id="password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button onClick={handlePasswordUpdate}>Update Password</button>
      </div>
    </>
  );
}

export default UserUpdateForm;
