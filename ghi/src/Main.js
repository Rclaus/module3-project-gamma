import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
// import "./css/Main.css";
import MainMap from "./MainMap";
import Teammate from "./Teammate";
import Card from "./Card";

function Main() {
  return (
    <div>
      <div className="row">
        <div className="col-md-12 mb-5 mt-5">
          <MainMap />
        </div>{" "}
      </div>{" "}
      <div className="row">
        <div className="col-md-12 mb-5">
          <Card />
        </div>{" "}
      </div>{" "}
      <div className="row">
        <div className="col-md-12 mb-5">
          <Teammate />
        </div>{" "}
      </div>{" "}
    </div>
  );
}

export default Main;
