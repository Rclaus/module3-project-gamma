import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import Modal from "react-bootstrap/Modal";
import { uploadToS3 } from "./photoProcessing/UploadToS3";
import { extractExifData } from "./photoProcessing/ExifExtractor";

function ImageUploadForm() {
  const { token } = useToken();
  const [accountData, setAccountData] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [file, setFile] = useState(null);
  const [exifData, setExifData] = useState({});
  const [isProcessing, setIsProcessing] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (token) {
      const fetchAccountData = async () => {
        const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
        const response = await fetch(accountUrl, {
          credentials: "include",
        }).then((response) => response.json());
        setAccountData(response.account);
      };
      fetchAccountData();
    }
  }, [token]);

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleFileSelect = async (e) => {
    setFile(e.target.files[0]);
    const data = await extractExifData(e.target.files[0]);
    setExifData(data || null);
  };

  const createPhotoDocument = async (fileName, imageUrl, exifData) => {
    if (!accountData) {
      return;
    }

    const photoDocument = {
      file_name: fileName,
      username: accountData.username,
      image_url: imageUrl,
      latitude: exifData ? exifData.latitude : "35.6814",
      longitude: exifData ? exifData.longitude : "-95.1564",
    };

    if (exifData) {
      photoDocument.latitude = exifData.latitude;
      photoDocument.longitude = exifData.longitude;
      photoDocument.timestamp = exifData.timestamp;
    }

    try {
      await fetch(`${process.env.REACT_APP_API_HOST}/api/photos`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(photoDocument),
      });
    } catch (error) {
      console.log("Error creating photo document:", error);
    }
  };

  const uploadFile = async () => {
    if (!file || !accountData) {
      return;
    }

    setIsProcessing(true);

    try {
      const Location = await uploadToS3(file, accountData.username);
      setImageUrl(Location);
      console.log("uploading to s3", Location);
      await createPhotoDocument(
        `${accountData.username}.${file.name}`,
        Location,
        exifData
      );
      if (!exifData) {
        alert(
          "The uploaded image did not contain EXIF data. Please customize your photo's location and description from within your gallery."
        );
      }
    } catch (error) {
      console.log("Error uploading to s3:", error);
    }

    setIsProcessing(false);
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  return (
    <>
      <div onClick={handleModalOpen}>Upload photo</div>
      <Modal show={showModal}>
        <Modal.Body>
          <div style={{ marginTop: "10px" }}>
            <h1>Test Image Upload</h1>
            <input type="file" onChange={handleFileSelect} />
            {file && (
              <div style={{ marginTop: "40px" }}>
                <button onClick={uploadFile} disabled={isProcessing}>
                  {isProcessing ? "Processing..." : "Process and Upload"}
                </button>
              </div>
            )}
            {imageUrl && (
              <div style={{ marginTop: "10px" }}>
                <img
                  src={imageUrl}
                  alt="uploaded"
                  style={{
                    maxWidth: "300px",
                    maxHeight: "300px",
                    width: "auto",
                    height: "auto",
                  }}
                />
              </div>
            )}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={handleModalClose}>Close Preview</button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ImageUploadForm;
