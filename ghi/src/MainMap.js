import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import { useEffect, useState } from "react";
import "./css/MainMap.css";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "animate.css";

const MainMap = () => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });
  const [map, setMap] = useState(null);
  const [markers, setMarkers] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [selectedMarkerIndex, setSelectedMarkerIndex] = useState(null);

  const handleImageClick = (index) => {
    setSelectedMarker(markers[index]);
    setSelectedMarkerIndex(index);
  };

  useEffect(() => {
    const fetchPhotos = async () => {
      const photosUrl = `${process.env.REACT_APP_API_HOST}/api/carousels/all`;
      const response = await fetch(photosUrl).then((response) =>
        response.json()
      );
      const carouselPics = response.carousels;
      const userMarkers = carouselPics.map((photo) => ({
        img: photo.image_url,
        lat: parseFloat(photo.latitude),
        lng: parseFloat(photo.longitude),
      }));
      setMarkers(userMarkers);
    };
    fetchPhotos();
  }, []);

  useEffect(() => {
    if (map && markers.length > 0) {
      const bounds = new window.google.maps.LatLngBounds();
      markers.forEach((marker) => {
        bounds.extend(marker);
      });
      map.fitBounds(bounds);
    }
  }, [map, markers]);

  useEffect(() => {
    if (map && selectedMarker) {
      map.panTo(selectedMarker);
    }
  }, [map, selectedMarker]);

  if (loadError) {
    return <div>Error loading Google Maps</div>;
  }

  return (
    <div>
      <div className="main-map-container">
        <div className="main-map-content animate__animated animate__backInDown">
          {!isLoaded ? (
            <h1>Loading...</h1>
          ) : (
            <GoogleMap
              mapContainerClassName="main-map"
              zoom={3}
              onLoad={(map) => setMap(map)}
              mapTypeId="satellite"
            >
              {map &&
                markers.map((marker, index) => (
                  <Marker
                    key={index}
                    position={marker}
                    map={map}
                    onClick={() => {
                      setSelectedMarker(marker);
                      setSelectedMarkerIndex(index);
                      handleImageClick(index);
                    }}
                    icon={{
                      url: "https://viaphotos.s3.us-east-2.amazonaws.com/marker-1.png",
                      scaledSize: new window.google.maps.Size(20, 30),
                    }}
                  />
                ))}
            </GoogleMap>
          )}
        </div>
      </div>
      <div className="main-carousel-container">
        <Carousel showThumbs={false} selectedItem={selectedMarkerIndex}>
          {markers.map((marker, index) => (
            <div key={index} onClick={() => handleImageClick(index)}>
              <img src={`${marker.img}`} alt="" />
            </div>
          ))}
        </Carousel>
      </div>
      <div className="row-company">
        <div className="col-md-6 mb-5">
          <img
            src="companyLogo.png"
            className="img-fluid"
            alt="company_logo_image"
          />
        </div>
        <div className="col-md-6 mb-5">
          <h1 className="text-center">About us</h1>
          VIA is an innovative app designed to revolutionize the way users
          interact with photos and location data. Our platform provides a
          seamless and intuitive experience, allowing individuals to
          effortlessly upload their photos, share them with others, and access
          valuable information associated with each image's location. With VIA,
          users can capture and store their favorite moments, whether it's an
          awe-inspiring landscape, a delicious meal, or a cherished memory. Our
          app combines the power of geolocation technology with a vibrant
          community, enabling users to not only share their experiences but also
          discover hidden gems, popular landmarks, and local insights.
        </div>
      </div>
      <div className="row-camera">
        <div className="col-md-6 mb-5">
          <h1 className="text-center">Our Values</h1>
          <p>
            <strong>INNOVATION:</strong> We embrace a culture of innovation,
            constantly seeking unique and creative solutions to revolutionize
            the way users interact with photos and location data. We strive to
            stay ahead of the curve and push the boundaries of what's possible.
          </p>
          <p>
            <strong>EMPOWERMENT:</strong> We believe in empowering our users to
            capture and preserve their favorite moments, whether it's an
            awe-inspiring landscape, a delightful meal, or a cherished memory.
            Our platform enables users to create meaningful connections with
            their experiences.
          </p>
        </div>
        <div className="col-md-6 mb-5">
          <img
            src="https://images.pexels.com/photos/593324/pexels-photo-593324.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
            className="img-fluid"
            alt="company_logo_image"
          />
        </div>
      </div>
    </div>
  );
};

export default MainMap;
