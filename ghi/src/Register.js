import { useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const Register = () => {
  const [showModal, setShowModal] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [fullName, setFullName] = useState("");
  const [isLoading, setLoading] = useState(false);
  const { register, login } = useToken();
  const navigate = useNavigate();

  const emailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  const createProfilePicsDocument = async (accountData) => {
    const photoDocument = {
      file_name: `${accountData.username}_profile_pic.png`,
      username: accountData.username,
      image_url: "profile_pic.png",
    };

    try {
      await fetch(`${process.env.REACT_APP_API_HOST}/api/profile-pics`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(photoDocument),
      });
    } catch (error) {
      console.log("Error creating profile pic document:", error);
    }
  };

  const handleRegistration = async (e) => {
    e.preventDefault();

    if (!emailReg.test(username)) {
      alert("Please enter a valid email address");
      return;
    }
    const accountData = {
      full_name: fullName,
      username: username,
      password: password,
    };

    try {
      setLoading(true);
      await register(
        accountData,
        `${process.env.REACT_APP_API_HOST}/api/accounts`
      );
      await login(username, password);
      await createProfilePicsDocument(accountData);
      navigate("/gallery");
    } catch (error) {
      console.error("Registration error:", error);
    } finally {
      setLoading(false);
    }

    e.target.reset();
  };

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button variant="primary" onClick={handleModalOpen}>
        Register
      </Button>

      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={(e) => handleRegistration(e)}>
            <input type="hidden" name="_token" value="" />
            <div className="form-group">
              <label className="control-label">E-Mail</label>
              <div>
                <input
                  name="username"
                  type="text"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="control-label">Password</label>
              <div>
                <input
                  name="password"
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="control-label">Full Name</label>
              <div>
                <input
                  name="fullName"
                  type="text"
                  value={fullName}
                  onChange={(e) => setFullName(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group">
              <div>
                <button
                  type="submit"
                  className="btn btn-success"
                  disabled={isLoading}
                >
                  {isLoading ? "Registering..." : "Register"}
                </button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Register;
