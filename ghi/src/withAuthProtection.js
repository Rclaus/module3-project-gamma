import React, { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function withAuthProtection(WrappedComponent) {
  return (props) => {
    const { token } = useToken();
    const [isLoading, setLoading] = useState(true);
    const [isAuthenticated, setAuthenticated] = useState(false);
    const navigate = useNavigate();
    const location = useLocation();
    useEffect(() => {
      const fetchAccountData = async () => {
        const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
        try {
          const response = await fetch(accountUrl, {
            credentials: "include",
          });
          const data = await response.json();
          if (data.account) {
            setAuthenticated(true);
          } else {
            setAuthenticated(false);
          }
        } catch (error) {
          console.error("Error validating token: ", error);
          setAuthenticated(false);
        } finally {
          setLoading(false);
        }
      };
      if (token) {
        fetchAccountData();
      } else {
        setLoading(false);
      }
    }, [token]);
    useEffect(() => {
      if (!isLoading && !isAuthenticated) {
        navigate("/", { replace: true, state: { from: location } });
      }
    }, [isLoading, isAuthenticated, navigate, location]);
    if (isLoading) {
      return null;
    }
    return isAuthenticated ? <WrappedComponent {...props} /> : null;
  };
}
