import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import { useEffect, useMemo, useState } from "react";
import "./css/Map.css";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const Map = () => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });
  const [map, setMap] = useState(null);
  const [accountData, setAccountData] = useState(null);
  const [markers, setMarkers] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [selectedMarkerIndex, setSelectedMarkerIndex] = useState(null);
  const center = useMemo(() => ({ lat: 27.9506, lng: -82.4572 }), []);
  const { token } = useToken();

  const handleImageClick = (index) => {
    setSelectedMarker(markers[index]);
    setSelectedMarkerIndex(index);
  };

  useEffect(() => {
    if (!token) return;
    const fetchAccountData = async () => {
      const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
      const response = await fetch(accountUrl, {
        credentials: "include",
      }).then((response) => response.json());
      setAccountData(response.account);
    };
    fetchAccountData();
  }, [token]);

  useEffect(() => {
    if (!accountData) return;
    const fetchPhotos = async () => {
      const photosUrl = `${process.env.REACT_APP_API_HOST}/api/photos/all/username/${accountData.username}`;
      const response = await fetch(photosUrl).then((response) =>
        response.json()
      );
      const userPhotos = response.photos;
      const userMarkers = userPhotos.map((photo) => ({
        img: photo.image_url,
        lat: parseFloat(photo.latitude),
        lng: parseFloat(photo.longitude),
      }));
      setMarkers(userMarkers);
    };
    fetchPhotos();
  }, [accountData]);

  useEffect(() => {
    if (map && selectedMarker) {
      map.panTo(selectedMarker);
    }
  }, [map, selectedMarker]);

  if (loadError) {
    return <div>Error loading Google Maps</div>;
  }

  return (
    <div>
      <div className="map-container">
        <div className="coordinates-container">
          <h2>Memories</h2>
          <ul>
            {markers.map((marker, index) => (
              <div key={index} onClick={() => handleImageClick(index)}>
                <img
                  src={`${marker.img}`}
                  alt=""
                  style={{
                    maxWidth: "500px",
                    maxHeight: "500px",
                    padding: "10%",
                    width: "auto",
                    height: "auto",
                  }}
                />
              </div>
            ))}
          </ul>
        </div>
        <div className="map-content">
          {!isLoaded ? (
            <h1>Loading...</h1>
          ) : (
            <GoogleMap
              mapContainerClassName="map"
              center={center}
              zoom={10}
              onLoad={(map) => setMap(map)}
            >
              {map &&
                markers.map((marker, index) => (
                  <Marker
                    key={index}
                    position={marker}
                    map={map}
                    onClick={() => {
                      setSelectedMarker(marker);
                      setSelectedMarkerIndex(index);
                      handleImageClick(index);
                    }}
                    icon={{
                      url: "https://viaphotos.s3.us-east-2.amazonaws.com/camera.png",
                      scaledSize: new window.google.maps.Size(50, 50), // Adjust the size as needed
                    }}
                  />
                ))}
            </GoogleMap>
          )}
        </div>
      </div>
      <div className="carousel-container">
        <Carousel showThumbs={false} selectedItem={selectedMarkerIndex}>
          {markers.map((marker, index) => (
            <div key={index}>
              <img src={`${marker.img}`} alt="" />
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default Map;
