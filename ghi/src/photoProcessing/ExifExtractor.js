import * as EXIF from "exif-js";

const formatTimestamp = (timestamp) => {
  const [date, time] = timestamp.split(" ");
  const [year, month, day] = date.split(":");
  const [hour, minute, second] = time.split(":");
  const formattedDate = new Date(
    Date.UTC(year, month - 1, day, hour, minute, second)
  );
  return formattedDate.toISOString().slice(0, -1);
};

export const extractExifData = (file) => {
  return new Promise((resolve, reject) => {
    EXIF.getData(file, function () {
      let lat = EXIF.getTag(this, "GPSLatitude");
      let lng = EXIF.getTag(this, "GPSLongitude");
      let timestamp = EXIF.getTag(this, "DateTimeOriginal");

      if (lat) {
        const [degrees, minutes, seconds] = lat;
        const direction = EXIF.getTag(this, "GPSLatitudeRef");
        lat = degrees + minutes / 60 + seconds / (60 * 60);
        if (direction === "S") {
          lat = -lat;
        }
      }

      if (lng) {
        const [degrees, minutes, seconds] = lng;
        const direction = EXIF.getTag(this, "GPSLongitudeRef");
        lng = degrees + minutes / 60 + seconds / (60 * 60);
        if (direction === "W") {
          lng = -lng;
        }
      }

      if (lat && lng && timestamp) {
        timestamp = formatTimestamp(timestamp); // Apply formatTimestamp function here
        resolve({
          latitude: lat,
          longitude: lng,
          timestamp: timestamp,
        });
      } else {
        resolve(null);
      }
    });
  });
};
