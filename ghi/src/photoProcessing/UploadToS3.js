import AWS from "aws-sdk";

AWS.config.update({
  accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY,
  region: process.env.REACT_APP_AWS_REGION,
  signatureVersion: process.env.REACT_APP_AWS_SIGNATURE_VER,
  maxRetries: 3,
});

const s3 = new AWS.S3();

export const uploadToS3 = async (file, username, isProfilePic = false) => {
  const fileName = isProfilePic
    ? `${username}.profile_pic.${file.name.split(".").pop()}`
    : `${username}.${file.name}`;
  const params = {
    Bucket: "viaphotos",
    Key: fileName,
    Body: file,
  };

  try {
    const { Location } = await s3.upload(params).promise();
    console.log("uploading to s3", Location);
    return Location;
  } catch (error) {
    console.log("Error uploading to s3:", error);
  }
};
