import "./css/Teammate.css";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";

function Teammate() {
  return (
    <div className="container">
      <div className="team">MEET OUR TEAM</div>
      <div className="row">
        <div className="col-12 col-sm-6 col-md-4 col-lg-3">
          <div className="our-team">
            <div className="picture">
              <img
                className="img-fluid"
                src="https://ca.slack-edge.com/T04JMHGLC1X-U04Q3MJS32M-7876609cccfc-512"
                alt=""
              />
            </div>
            <div className="team-content">
              <h3 className="name">Matt Hoadley</h3>
              <h4 className="title">Web Developer</h4>
            </div>
            <ul className="social">
              <li>
                <a href="https://www.linkedin.com/in/mhoadley/">
                  <FontAwesomeIcon icon={faLinkedin} aria-hidden="true" />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3">
          <div className="our-team">
            <div className="picture">
              <img
                className="img-fluid"
                src="https://ca.slack-edge.com/T04JMHGLC1X-U04PQUWJ92P-fcfc4844d5cb-512"
                alt=""
              />
            </div>
            <div className="team-content">
              <h3 className="name">Rob Claus</h3>
              <h4 className="title">Web Developer</h4>
            </div>
            <ul className="social">
              <li>
                <a href="https://www.linkedin.com/in/rob-claus/">
                  <FontAwesomeIcon icon={faLinkedin} aria-hidden="true" />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3">
          <div className="our-team">
            <div className="picture">
              <img
                className="img-fluid"
                src="https://ca.slack-edge.com/T04JMHGLC1X-U04QEUBS35X-efc21096dde4-512"
                alt=""
              />
            </div>
            <div className="team-content">
              <h3 className="name">Thomas Laurick</h3>
              <h4 className="title">Web Developer</h4>
            </div>
            <ul className="social">
              <li>
                <a href="https://www.linkedin.com/in/unsweet-t/">
                  <FontAwesomeIcon icon={faLinkedin} aria-hidden="true" />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3">
          <div className="our-team">
            <div className="picture">
              <img
                className="img-fluid"
                src="https://ca.slack-edge.com/T04JMHGLC1X-U04Q3MM8HJM-d6f2ced444d8-512"
                alt=""
              />
            </div>
            <div className="team-content">
              <h3 className="name">Huilin Li</h3>
              <h4 className="title">Web Developer</h4>
            </div>
            <ul className="social">
              <li>
                <a href="https://www.linkedin.com/in/huilin-li/">
                  <FontAwesomeIcon icon={faLinkedin} aria-hidden="true" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Teammate;
