import "./css/Footer.css";

const Footer = () => {
  const year = new Date().getFullYear();

  return (
    <div className="footer">
      <footer>{`Copyright © Destination Devs ${year}`}</footer>
    </div>
  );
};

export default Footer;
