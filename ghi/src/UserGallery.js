import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import { useEffect, useState } from "react";
import "./css/UserGallery.css";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import UpdatePhoto from "./UpdatePhoto";
import "animate.css";

const Map = () => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });
  const [map, setMap] = useState(null);
  const [accountData, setAccountData] = useState(null);
  const [markers, setMarkers] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [selectedMarkerIndex, setSelectedMarkerIndex] = useState(null);
  const { token } = useToken();

  const handleImageClick = (index) => {
    setSelectedMarker(markers[index]);
    setSelectedMarkerIndex(index);
  };

  useEffect(() => {
    if (!token) return;
    const fetchAccountData = async () => {
      const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
      const response = await fetch(accountUrl, {
        credentials: "include",
      }).then((response) => response.json());
      setAccountData(response.account);
    };
    fetchAccountData();
  }, [token]);

  useEffect(() => {
    if (!accountData) return;
    const fetchPhotos = async () => {
      const photosUrl = `${process.env.REACT_APP_API_HOST}/api/photos/all/username/${accountData.username}`;
      const response = await fetch(photosUrl).then((response) =>
        response.json()
      );
      const userPhotos = response.photos;
      const userMarkers = userPhotos.map((photo) => ({
        img: photo.image_url,
        lat: parseFloat(photo.latitude),
        lng: parseFloat(photo.longitude),
        file_name: photo.file_name,
        description: photo.description ?? "",
      }));
      setMarkers(userMarkers);
    };
    fetchPhotos();
  }, [accountData]);

  useEffect(() => {
    if (map && markers.length > 0) {
      const bounds = new window.google.maps.LatLngBounds();
      markers.forEach((marker) => {
        bounds.extend(marker);
      });
      map.fitBounds(bounds);
    }
  }, [map, markers]);

  useEffect(() => {
    if (map && selectedMarker) {
      map.panTo(selectedMarker);
    }
  }, [map, selectedMarker]);

  if (loadError) {
    return <div>Error loading Google Maps</div>;
  }

  return (
    <div>
      <div className="update-photo-container">
        <UpdatePhoto markers={markers} />
      </div>
      <div className="header">
        <h2 className="animate__animated animate__jello">
          Welcome to Your personal gallery: Explore the world with photos!
        </h2>
      </div>

      <div className="map-container animate__animated animate__backInDown">
        <div className="coordinates-container">
          <h2>Memories</h2>
          <ul>
            {markers.map((marker, index) => (
              <div key={index} onClick={() => handleImageClick(index)}>
                <img
                  src={`${marker.img}`}
                  alt=""
                  style={{
                    maxWidth: "500px",
                    maxHeight: "500px",
                    padding: "10%",
                    width: "auto",
                    height: "auto",
                  }}
                />
              </div>
            ))}
          </ul>
        </div>
        <div className="map-content">
          {!isLoaded ? (
            <h1>Loading...</h1>
          ) : (
            <GoogleMap
              mapContainerClassName="map"
              zoom={10}
              onLoad={(map) => setMap(map)}
              mapTypeId="satellite"
            >
              {map &&
                markers.map((marker, index) => (
                  <Marker
                    key={index}
                    position={marker}
                    map={map}
                    onClick={() => {
                      setSelectedMarker(marker);
                      setSelectedMarkerIndex(index);
                      handleImageClick(index);
                    }}
                    icon={{
                      url: "https://viaphotos.s3.us-east-2.amazonaws.com/marker-1.png",
                      scaledSize: new window.google.maps.Size(20, 30),
                    }}
                  />
                ))}
            </GoogleMap>
          )}
        </div>
      </div>
      <div className="carousel-container">
        <Carousel showThumbs={false} selectedItem={selectedMarkerIndex}>
          {markers.map((marker, index) => (
            <div
              key={index}
              onClick={() => {
                handleImageClick(index);
                setSelectedMarkerIndex(index);
              }}
            >
              <img src={`${marker.img}`} alt="" />
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default Map;
