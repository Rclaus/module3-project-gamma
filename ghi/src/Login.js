import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

function Login() {
  const [showModal, setShowModal] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!username) {
      setUsernameError("email field is required");
      return;
    }

    if (!password) {
      setPasswordError("Password is required");
      return;
    }

    login(username, password);
    e.target.reset();
    navigate("/gallery");
  };

  useEffect(() => {
    setUsername("");
    setPassword("");
  }, []);

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button variant="primary" onClick={handleModalOpen}>
        Login
      </Button>
      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={(e) => handleSubmit(e)}>
            <input type="hidden" name="_token" value="" />
            <div className="form-group">
              <label className="control-label">E-Mail Address</label>
              <div>
                <input
                  name="username"
                  type="text"
                  value={username}
                  onChange={(e) => {
                    setUsername(e.target.value);
                    setUsernameError("");
                  }}
                />
                {usernameError && (
                  <span style={{ color: "red" }}>{usernameError}</span>
                )}{" "}
              </div>
            </div>
            <div className="form-group">
              <label className="control-label">Password</label>
              <div>
                <input
                  name="password"
                  type="password"
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value);
                    setPasswordError("");
                  }}
                />
                {passwordError && (
                  <span style={{ color: "red" }}>{passwordError}</span>
                )}{" "}
              </div>
            </div>
            <div className="form-group">
              <div>
                <div className="checkbox">
                  <label>
                    <input type="checkbox" name="remember" /> Remember Me
                  </label>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-success">
                  Login
                </button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Login;
