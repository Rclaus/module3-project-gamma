import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import "./css/Nav.css";
import ImageUploadForm from "./ImageUploadForm";

function Nav() {
  const { logout, token } = useToken();
  const [accountData, setAccountData] = useState(null);

  const handleLogout = () => {
    logout();
    alert("Successfully logged out");
  };

  useEffect(() => {
    if (token) {
      const handleFetch = async () => {
        const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
        const response = await fetch(accountUrl, {
          credentials: "include",
        }).then((response) => response.json());
        setAccountData(response.account);
      };
      handleFetch();
    }
  }, [token]);

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <div className="nav">
          <div className="nav-title-container">
            <NavLink className="navbar-brand" to="/">
              <img
                src="https://viaphotos.s3.us-east-2.amazonaws.com/companylogo3.png"
                alt="logo"
                width="40vw"
                height="40vh"
              />
            </NavLink>
          </div>
          <div className="dropdown">
            <button
              className="btn dropdown-toggle"
              type="button"
              color="black"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              Menu
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              {token ? (
                <>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/">
                      Home
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/gallery">
                      My Photos
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <div className="dropdown-item">
                      <ImageUploadForm />
                    </div>
                  </li>
                </>
              ) : (
                <>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/">
                      Home
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <div className="dropdown-item">
                      <Login />
                    </div>
                  </li>
                  <li className="nav-item">
                    <div className="dropdown-item">
                      <Register />
                    </div>
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
        {token && (
          <div className="welcome-message">
            Welcome, {accountData?.username}!
          </div>
        )}
        <div className="nav">
          <div className="btn-group dropstart">
            {token ? (
              <>
                <button
                  className="btn dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  justified-right="true"
                >
                  <img
                    src="https://viaphotos.s3.us-east-2.amazonaws.com/profile_pic.png"
                    className="profile_pic"
                    alt="Profile Pic"
                    width="40"
                    height="40"
                  />
                </button>
                <ul
                  className="dropdown-menu"
                  aria-labelledby="dropdownMenuButton1"
                >
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/gallery">
                      My Photos
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <div className="dropdown-item">
                      <ImageUploadForm />
                    </div>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      className="dropdown-item"
                      to="/"
                      onClick={handleLogout}
                    >
                      Logout
                    </NavLink>
                  </li>
                </ul>
              </>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
